var $input1 = $('#input1'),
	$textarea1 = $('#textarea1'),
	$select1 = $('#select1'),
	$buttonReset = $('button[type="reset"]'),
	$warningText = $('.warning');

// jQuery UI
$buttonReset.button();

// Скрывать предупреждение при вводе
$input1.on('input', function() {
	$warningText.css('visibility', 'hidden');
});

$textarea1.on('input', function() {
	$warningText.css('visibility', 'hidden');
});

// Очищение полей кнопкой Отменить
$buttonReset.click(function() {
	$input1.val('');
	$textarea1.val('');
});

// Формирование списка групп в Select
getGroup();

function getGroup() {
	fetch('http://localhost:3000/groups/')
		.then(function(response) {
			return response.json();
		})
		.then(function(group) {
			setGroup(group);
		})
		.catch(function(ex) {
			console.log('parsing failed: ' + ex);
		});
}

function setGroup(groups) {

	groups.forEach(function(groupElem) {
		var condition = 0;

		[].forEach.call(document.getElementById('select1'), function(selectElem) {
			
			if(groupElem.title === selectElem.value) {
				condition = 1;
				return;
			}
			
		});

		if(condition) return;

		var newOption = document.createElement('option');
		$(newOption).text(groupElem.title);
		$select1.append(newOption);	
	});

}

function refreshGroup(x) {
	/*
	Входные параметры:
	x - выбранный option у select
	*/
	var x = x || undefined;

	if (x) x.remove();

	setTimeout(getGroup, 500)
}

// Функция проверки на пустоту
function isEmpty(field) {
	/*
	Выходные параметры:
	Boolean: true (поле не содержит символ),
		false (поле содержит символ)
	*/
	return field.val().match(/\S/) === null;
}

// Функция исправления текста
function fixText() {
	/*
	Входные параметры:
		func - функция для исполнения,
		arg - один аргумент для этой функции.
			Пример:
			(func, arg, func, arg, и т.д.)
	*/

	for(var i = 0; i < arguments.length; i += 2) {
		arguments[i](arguments[i + 1]);
	}

}

// Функция удаления из текста тегов
function removeTag(field) {
	/*
	Входные параметры:
	field - поле для ввода текста
	*/
	field.val( field.val().replace(/\[.+\]/, '') );
}

// Функция обрезания пробелов в начале и в конце текста, удаление
// лишних пробелов в середине
function removeSpacing(field) {
	/*
	Входные параметры:
	field - поле для ввода текста
	*/
	field.val( field.val().replace(/^\s+/, '').replace(/\s+$/, '')
		.replace(/\s{2,}/, ' ') );
}

// Функция работы с текстом, написанным КАПС ЛОКОМ
function fixCapsLock(field) {
	/*
	Входные параметры:
	field - поле для ввода текста
	*/
	var sentences = [];
	
	field.val( field.val().toLowerCase() );
	// разбить текст на массив предложений
	sentences = field.val().match(/[^.]+\.?( *|$)/g);

	// сделать первую букву заглавной
	sentences = sentences.map(function(e) {
		return e.charAt(0).toUpperCase() + e.slice(1);
	});

	field.val( sentences.join('') );
}