var windowLoginForm = document.createElement('div'),
	overlayLoginForm = document.createElement('div'),
	loginForm = document.createElement('div'),
	h3 = document.createElement('h3'),
	inputLogin = document.createElement('input'),
	inputPassword = document.createElement('input'),
	inputSubmit = document.createElement('input'),
	currentUser = '',
	idUser = 0,
	passUser = '',
	result = false,
	resultLogin = false,
	resultPassword = false;

$(document).ready(function() {
	$login = $('#login');
	var quit = document.getElementById('quit');

	// Клик по кнопке Выход
	quit.addEventListener('click', function() {
		fetch('http://localhost:3000/users/' + idUser, {
			method: 'put',
			headers: {
				  'Accept': 'application/json',
				  'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					"login": currentUser,
					"password": passUser,
					"switch": "off",
					"id": idUser
			   })
			})
			.then(function(response) {
				return response.json();
			})
			.catch(function(ex) {
				console.log('parsing failed: ' +ex);
			});

		// Отображение формы для входа
		$(windowLoginForm).css('visibility', 'visible');

		$(inputLogin).val('');
		$(inputPassword).val('');
	});

});

$(windowLoginForm).appendTo( $('body') )
				  .addClass('window-login-form');
$(overlayLoginForm).appendTo( $(windowLoginForm) )
				   .addClass('overlay-login-form');
$(loginForm).appendTo( $(windowLoginForm) )
			.addClass('login-form');
$(h3).appendTo( $(loginForm) )
	 .text('Форма входа');
$(inputLogin).appendTo( $(loginForm) )
			 .attr({
			 	'type': 'text',
			 	'placeholder': 'Логин (admin)',
			 	'pattern': '[a-zA-Z0-9]+'
			 });
$(inputPassword).appendTo( $(loginForm) )
				.attr({
					'type': 'password',
			 		'placeholder': 'Пароль (admin)'
				});
$(inputSubmit).appendTo( $(loginForm) )
			  .attr({
				  'type': 'submit',
			 	  'value': 'Войти'
			  })
			  .click(function() {
			  		result = checkLoginAndPassword();

			  		setTimeout(function() {		  			

						if (result) {
							currentUser = $(inputLogin).val();
							$login.text(currentUser);

					  		hideForm();

					  		fetch('http://localhost:3000/users/' + idUser, {
								method: 'put',
								headers: {
									  'Accept': 'application/json',
									  'Content-Type': 'application/json'
									},
									body: JSON.stringify({
										"login": currentUser,
										"password": $(inputPassword).val(),
										"switch": "on",
										"id": idUser
								   })
								})
								.then(function(response) {
									return response.json();
								})
								.catch(function(ex) {
									console.log('parsing failed: ' +ex);
								});

						}

			  		}, 500);

			  });

fetch('http://localhost:3000/users')
		.then(function(response) {
			return response.json();
		})
		.then(function(users) {
			var isOn = false,
				arr = [];

			arr = users.filter(function(u) {
				currentUser = u.login;
				idUser = u.id;
				passUser = u.password;
				$login.text(currentUser);
				
				return u.switch === 'on';
			});

			if (arr[0]) {
				isOn = true;
				hideForm();
			} else {
				$(windowLoginForm).css('visibility', 'visible');
			}

		})
		.catch(function(ex) {
			console.log('parsing failed: ' + ex);
		});



function checkLoginAndPassword() {

	fetch('http://localhost:3000/users')
		.then(function(response) {
			return response.json();
		})
		.then(function(users) {
			// Находим в базе логин
			resultLogin = users.some(function(u) {
				// Сохраняем id данного пользователя
				idUser = u.id;

				return u.login === $(inputLogin).val();
			});
			
			// Если был присвоен id, после нахождения логина
			if (idUser) {
				resultPassword = users[idUser - 1].password === $(inputPassword).val()
				
				if (resultLogin && resultPassword) result = true;
			}
	
		})
		.catch(function(ex) {
			console.log('parsing failed: ' + ex);
		});

}

// Скрыть форму для входа и отобразить основной контент
function hideForm() {
	$(windowLoginForm).css('visibility', 'hidden');
	$('body').css('visibility', 'visible');
}